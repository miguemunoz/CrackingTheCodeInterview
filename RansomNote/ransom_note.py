def ransom_note(magazine, rasom):
    for x in rasom:
        if not x in magazine:
            return False
        else:
            magazine.remove(x)
    return True
           

m, n = map(int, input().strip().split(' '))
magazine = input().strip().split(' ')
ransom = input().strip().split(' ')
answer = ransom_note(magazine, ransom)
if(answer):
    print("Yes")
else:
    print("No")